CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This modules provides the bridge between content and code. It allows developers
to use specific entities (like the news-overview page) in their module without
knowing the ID of that piece of content.

Use cases:

* I don't want to save the node id of the homepage in config, instead I tag
a node with "Homepage" and System Tags will take care of the rest.
* I want to show a list of popular news items in the sidebar on
the news overview page, so I tag a node with "News overview" and configure
the "System Tags"-condition of that list block
to use the "News overview"-System Tag.
* The url of an article should start with the alias of
the overview page (/articles/my-news-article). This can be done by
using the correct token in the Pathauto pattern for articles.

REQUIREMENTS
------------

This module has no any specific dependency.

INSTALLATION
------------

Installation is very basic download and install the module at admin/modules.

CONFIGURATION
-------------

The setup is rather easy:

* Add a reference field to the 'System Tag'-entity
(eg. field_page_system_tag on Node type 'Page');
* Mark a certain entity with a tag via the field you just created;
* Take a look in `Drupal\system_tags\Config\SystemPageConfigOverrider::loadOverrides`
for how to use this module
in your code.

Twig support
------------

A Twig function is included: system_tag_url. It allows 3 parameters:
the System Tag ID (required),
the entity type (default: node) and an array of options that will be passed
to the `\Drupal\Core\Entity\EntityInterface::toUrl()`-function.
Eg. `<a href="{{- system_tag_url('homepage') -}}">`.

Token support
-------------

For every entity type which has a reference field to System Tag, a token will
be available. The structure will be
the following: `system_tags:ENTITY_TYPE--TAG_ID`.
Eg. `[system_tags:node--homepage]`.

Theming support
---------------

When you enable the submodule `system_tags_theme`, all the configured
System Tags, set for the current node, will be available as suggestions.
This allows a themer to override the `node.html.twig`
-template, based on the System Tag.
Eg. `node--system-tag--homepage.html.twig`.

MAINTAINERS
-----------

  * LammensJ - https://www.drupal.org/u/lammensj
