<?php

namespace Drupal\system_tags\Config;

/**
 * Defines SystemTagDefinitions class.
 *
 * @package Drupal\system_tags\Config
 */
class SystemTagDefinitions {

  const TAG_ACCESS_DENIED = 'access_denied';
  const TAG_HOMEPAGE = 'homepage';
  const TAG_PAGE_NOT_FOUND = 'page_not_found';

}
