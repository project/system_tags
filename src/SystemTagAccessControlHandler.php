<?php

namespace Drupal\system_tags;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the system tag entity type.
 *
 * @see \Drupal\system_tags\Entity\SystemTag
 */
class SystemTagAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view system tags');
    }

    return AccessResult::allowedIfHasPermission($account, 'administer system tags');
  }

}
