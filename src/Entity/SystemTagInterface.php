<?php

namespace Drupal\system_tags\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the SystemTagInterface Interface.
 *
 * @package Drupal\system_tags\Entity
 */
interface SystemTagInterface extends ConfigEntityInterface {

}
