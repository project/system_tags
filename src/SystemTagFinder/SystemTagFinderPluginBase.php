<?php

namespace Drupal\system_tags\SystemTagFinder;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\system_tags\SystemTagHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the SystemTagFinderPluginBase class.
 *
 * @package Drupal\system_tags\SystemTagFinder
 */
abstract class SystemTagFinderPluginBase extends PluginBase implements SystemTagFinderInterface, ContainerFactoryPluginInterface {

  /**
   * The system tag helper.
   *
   * @var \Drupal\system_tags\SystemTagHelperInterface
   */
  protected $systemTagHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * SystemTagFinderPluginBase constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\system_tags\SystemTagHelperInterface $system_tag_helper
   *   The system tag helper.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SystemTagHelperInterface $system_tag_helper,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->systemTagHelper = $system_tag_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('system_tags.system_tag_helper'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function findByTag($systemTagId, $langcode = NULL) {
    $entities = [];
    $entityTypeId = $this->pluginDefinition['entity_type'];

    if ($fields = $this->systemTagHelper->getReferenceFieldNames($entityTypeId)) {
      $langcode = $langcode ?: $this->languageManager->getCurrentLanguage()->getId();
      $storage = $this->entityTypeManager->getStorage($entityTypeId);

      $query = $storage->getQuery()
        ->accessCheck()
        ->sort('changed', 'DESC', $langcode);
      $orCondition = $query->orConditionGroup();
      foreach ($fields as $field) {
        $condition = $query->andConditionGroup()
          ->exists($field)
          ->condition($field, $systemTagId);
        $orCondition->condition($condition);
      }
      $query->condition($orCondition);
      $query->addTag($entityTypeId . '_access');
      $entity_type = $this->entityTypeManager->getDefinition($entityTypeId);
      // Check if the entity type only returns published entities, when the
      // entity type has a status property.
      if ($entity_type->getKey('status') !== FALSE) {
        $query->condition($entity_type->getKey('status'), TRUE);
      }
      // Add langcode as metadata to the query. Custom hook_node_access_records
      // implementations will use the current language instead of falling back
      // to the default language.
      $query->addMetaData('langcode', $langcode);

      if ($result = $query->execute()) {
        $entities = $storage->loadMultiple($result);

        // Replace entities with translated version.
        foreach ($entities as &$entity) {
          if (($entity instanceof TranslatableInterface) && $entity->hasTranslation($langcode)) {
            $entity = $entity->getTranslation($langcode);
          }
        }
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function findOneByTag($systemTagId, $langcode = NULL) {
    $entities = $this->findByTag($systemTagId, $langcode);

    return reset($entities) ?: NULL;
  }

}
