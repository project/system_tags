<?php

namespace Drupal\system_tags\SystemTagFinder;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines the SystemTagFinderManagerInterface Interface.
 *
 * @package Drupal\system_tags\SystemTagFinder
 */
interface SystemTagFinderManagerInterface extends PluginManagerInterface {

}
