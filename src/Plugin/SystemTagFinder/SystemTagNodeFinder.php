<?php

namespace Drupal\system_tags\Plugin\SystemTagFinder;

use Drupal\system_tags\SystemTagFinder\SystemTagFinderPluginBase;

/**
 * Defines the SystemTagNodeFinder class.
 *
 * @package Drupal\system_tags\Plugin\SystemTagFinder
 *
 * @SystemTagFinder(
 *   id = "system_tag_node_finder",
 *   entity_type = "node"
 * )
 */
class SystemTagNodeFinder extends SystemTagFinderPluginBase {
}
