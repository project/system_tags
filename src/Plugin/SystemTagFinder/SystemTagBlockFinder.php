<?php

namespace Drupal\system_tags\Plugin\SystemTagFinder;

use Drupal\system_tags\SystemTagFinder\SystemTagFinderPluginBase;

/**
 * Defines the SystemTagBlockFinder class.
 *
 * @package Drupal\system_tags\Plugin\SystemTagFinder
 *
 * @SystemTagFinder(
 *   id = "system_tag_block_finder",
 *   entity_type = "block_content"
 * )
 */
class SystemTagBlockFinder extends SystemTagFinderPluginBase {
}
